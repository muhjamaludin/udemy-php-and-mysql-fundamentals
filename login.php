<?php

include 'account.php';

// login
if (isset($_POST['login'])) {
    $status = $db->loginUser($_POST['user'], $_POST['pass']);
    echo ($status) ? header('location:./home.php') : "Login failed";
}

// sign up
if (isset($_POST['signup'])) {
    $status = $db->signupUser($_POST['s_user'], $_POST['s_pass']);
    echo ($status) ? header('location:./login.php') : "Sign Up failed";
}

?>

<html>
    <head>
        <style>
            input {
                display: block;
                margin-top: 10px;
            }
        </style>
    </head>

    <body>
        <h3><?= (isset($_SESSION['user'])) ? $_SESSION['user'] : ''; ?></h3>
        
        <form action="./login.php" method="POST">
            <input type="text" name="user">
            <input type="password" name="pass">
            <input type="submit" name="login" value="Log In">
        </form>
        <hr>
        <form action="./login.php" method="POST">
            <input type="text" name="s_user">
            <input type="password" name="s_pass">
            <input type="submit" name="signup" value="Sign Up">
        </form>
    </body>
</html>