<?php

class Product {

    protected $price;
    protected $productName;
    protected $currency;
    protected $quantity;

    public function __construct($price, $productName, $currency, $quantity) {
        $this->price = $price;
        $this->productName = $productName;
        $this->currency = $currency;
        $this->quantity = $quantity;
    }

    public function setPrice($val) {
        $this->price = $val;
    }

    public function setProductName($val) {
        $this->productName = $val;
    }

    public function setCurrency($val) {
        $this->currency = $val;
    }

    public function setQuantity($val) {
        $this->quantity = $val;
    }

    // Getters
    public function getPrice() {
        return $this->price;
    }

    public function getProductName() {
        return $this->productName;
    }

    public function getCurrencty() {
        return $this->currency;
    }

    public function getQuantity() {
        return $this->quantity;
    }
}

$product = new Product(100.00, 'Table Noir', 'euro', 30);
$product2 = new Product(34.00, 'Chari Noir', 'euro', 35);

?>

<html>
    <body>
        
        <div style="float: left; margin-right: 40px;">
            <h3><?= $product->getProductName(); ?></h3>
                <div style="height: 250px; width: 250px; background-color: maroon; margin: 10px 0;">

                </div>
            <p>This is the description. Available: <?= $product->getQuantity(); ?></p>
            <h4>Starting price will be: <b><?= $product->getPrice()." ". $product->getCurrencty(); ?></b></h4>
        </div>
        <div style="float: left;">
        <h3><?= $product2->getProductName(); ?></h3>
                <div style="height: 250px; width: 250px; background-color: maroon; margin: 10px 0;">

                </div>
            <p>This is the description. Available: <?= $product2->getQuantity(); ?></p>
            <h4>Starting price will be: <b><?= $product2->getPrice()." ". $product->getCurrencty(); ?></b></h4>
        </div>

    </body>
</html>