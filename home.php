<?php

session_start();

if (!isset($_SESSION['user'])) {
    header('location:./login.php');
} else { ?>
    <h3>Welcome to the home page <?= $_SESSION['user']; ?></h3>
<?php }

?>
