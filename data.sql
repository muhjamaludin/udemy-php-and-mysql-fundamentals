CREATE DATABASE clients;

CREATE TABLE clients (
    client_id INT AUTO_INCREMENT,
    firstname VARCHAR(50),
    lastname VARCHAR(50),
    rating INT,
    PRIMARY KEY(id)
);