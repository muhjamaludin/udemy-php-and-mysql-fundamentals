<?php

class Person {
    
    protected $name;
    protected $lastname;
    protected $height;

    public function __construct($name, $lastname, $height) {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->height = $height;
        // echo "object person successfully <br>";
    }

    public function sayHello() {
        echo $this->name. " ". $this->lastname;
    }

    public function setName($value) {
        echo $this->name . " is the initial value. <br>";
        $this->name = $value;
        echo $this->name . " changed successfully.";
    }

    public function getName() {
        return $this->name;
    }
}

class Developer extends Person {

    protected $skill;
    protected $job;

    public function __construct($skill, $job, $name, $lastname, $height) {
        parent::__construct($name, $lastname, $height);
        $this->name = $name;
        $this->lastname = $lastname;
        $this->height = $height;
        $this->skill = $skill;
        $this->job = $job;
        echo "Developed created <br>";
    }

    public function getStats() {
        echo "Name : ". $this->name . "<br>". " Lastname : " . $this->lastname;
        echo "<br>Skill : ". $this->skill. "<br>". " Job : ". $this->job;
    }
}

$d = new Developer("Web Developer", "Part time", "Mike", "Assassins", 192);

$d->getStats();