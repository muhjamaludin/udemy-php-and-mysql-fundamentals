<?php

class UserOps {

    protected $dbName, $dbUser, $dbPass, $dbHost, $conn, $query;
    public $username;

    public function UserOps($host, $user, $pass, $name) {
        $this->dbName = $name;
        $this->dbPass = $pass;
        $this->dbHost = $host;
        $this->dbUser = $user;
        $this->conn = $this->connect($this->dbHost, $this->dbUser, $this->dbPass, $this->dbName);
    }

    public function connect($host, $user, $pass, $db) {
        $c = mysqli_connect($host, $user, $pass, $db);
        return ($c) ? $c : false;
    }

    public function validate($user, $pass) {
        $errors = 0;
        if (preg_match('/\\s+/', $user) || empty($user)) {
            $errors += 1;
        }
        if (preg_match('/\\s+/', $pass) || empty($pass)) {
            $errors += 1;
        }
        return ($errors > 0) ? 0 : 1;
    }   

    public function loginUser($user, $pswd) {
        $status = $this->validate($user, $pswd);
        if ($status) {
            $this->username = $user;
            session_start();
            $_SESSION['user'] = $this->username;
            $pswd = md5($pswd);
            $user = md5($user);
            $user = mysqli_real_escape_string($this->conn, $user);
            $pswd = mysqli_real_escape_string($this->conn, $pswd);

            $query = "SELECT * FROM client WHERE client_username='$user' AND client_password='$pswd'";
            $RESULT = mysqli_query($this->conn, $query);
            $count = (mysqli_num_rows($RESULT) == 1) ? 1 : 0;
            return ($count) ? 1 : 0;

        } else {
            return 0;
        }
    }

    public function signupUser($user, $pswd) {
        $status = $this->validate($user, $pswd);
        if ($status) {
            $this->username = $user;
            $pswd = md5($pswd);
            $user = md5($user);
            $user = mysqli_real_escape_string($this->conn, $user);
            $pswd = mysqli_real_escape_string($this->conn, $pswd);

            $query = "SELECT * FROM client WHERE client_username='$user' AND client_password='$pswd'";
            $RESULT = mysqli_query($this->conn, $query);
            $count = (mysqli_num_rows($RESULT) == 1) ? 1 : 0;
            if ($count) {
                return 0; // user already exists
            }
            // sig up user
            $query = "INSERT INTO client VALUES (null, '$user', '$pswd')";
            $result = mysqli_query($this->conn, $query);
            return ($result) ? 1 : 0;

        } else {
            return 0;
        }
    }
}

$db = new UserOps('localhost', 'root', '', 'clients');