
# Project { comments } 

# define a class { use a small name } 
    # define properties 
        # username { public }
        # db connection params

    # define a constructor params 

    # define methods 
        # validation method 
        # encryption/decryption function 
        # login function 
        # sign up function 
        # setter/getters

# define a class for the database 
    # define the properties 
        # dbname 
        # dbhost 
        # dbuser 
        # dbpass 

    # define the methods 
        # connect function { db properties } 
        # query function 
        # fetch function { store them in an array } 
        $ setters/getters 

# generate class objects 
# use them to some other page { you need the include function } 